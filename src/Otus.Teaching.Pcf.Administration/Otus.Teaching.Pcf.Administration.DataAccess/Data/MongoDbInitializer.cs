﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data;

public class MongoDbInitializer : IDbInitializer
{
    private readonly IMongoCollection<Employee> _employees;
    private readonly IMongoCollection<Role> _roles;
    public MongoDbInitializer(IMongoCollection<Employee> employees, IMongoCollection<Role> roles)
    {
        _employees = employees;
        _roles = roles;
    }
    public void InitializeDb()
    {
        _employees.DeleteMany(_ => true);
        _roles.DeleteMany(_ => true);
        _employees.InsertMany(FakeDataFactory.Employees);
        _roles.InsertMany(FakeDataFactory.Roles);
    }
}