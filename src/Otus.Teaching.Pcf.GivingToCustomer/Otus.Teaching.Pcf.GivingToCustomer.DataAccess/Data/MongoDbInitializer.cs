﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;

public class MongoDbInitializer : IDbInitializer
{
    private readonly IMongoCollection<Preference> _preferences;
    private readonly IMongoCollection<Customer> _customers;
    public MongoDbInitializer(IMongoCollection<Preference> preferences, IMongoCollection<Customer> customers)
    {
        _preferences = preferences;
        _customers = customers;
    }
    public void InitializeDb()
    {
        _preferences.DeleteMany(_ => true);
        _customers.DeleteMany(_ => true);
        _preferences.InsertMany(FakeDataFactory.Preferences);
        _customers.InsertMany(FakeDataFactory.Customers);
    }
}